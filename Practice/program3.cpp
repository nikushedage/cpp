/* Imagine a publish company which does marketing for book and audio cassete versions. Create a class publication that stores the title and price of publication. From this class derive two classes . book which adds a page count,and tape,which adds a playing time in minutes . Write a program that instantiates the book and the tape classes , allows user to enter the data and display the data members. if an exception is caught, replace all the data member value with zero values.*/


#include<iostream>
#include<cstring>
#include<exception>
using namespace std; 

class publication{
    protected:
    string title;
    float price;
  public:
  publication(string t ="No title", float p =0.0)  
  {
    title =t;
    price=p;
  }
};
class books:public publication{
    int pages;
   public:
   books(string t="no title",float p=0.0,int pa=0):publication(t,p)
   {
    pages=pa;
   } 
   void getb()
   {
    cout<<"Enter the details of the book:"<<endl;
    cout<<"Enter the title of the book:"<<endl;
    cin>>title;
    cout<<"Enter the price of the book:"<<endl;
    cin>>price;
    cout<<"Enter the pages of the book:"<<endl;
    cin>>pages;
    try
    {
        if(pages>500 && pages<1500)
        {
            if(price>100 && price<2000)
            {
            displayb();
            }
        }
    
    else throw(pages);
    }
   catch(int i){
    cout<<"Caught the exeption in the functiion of the book"<<endl;
    cout<<"You entered an invalid data"<<endl;
    title="0";
    price=0.0;
    pages=0;
    displayb();
    throw;
   }
}
void displayb()
{
    cout<<"The book you enetred is"<<endl<<endl;
    cout<<"the enterd title is:"<<title<<endl;
    cout<<"the enterd price is:"<<price<<endl;
    cout<<"the enterd pages are:"<<pages<<endl;
}
};
class tape:public publication{
    float min;
    public:
    tape(string t="no title",float p=0.0,float m=0.00):publication(t,p)
    {
        min=m;
    }
     void gett()
    {
        cout<<"Enter the details of the tape"<<endl;
        cout<<"enter the title of the tape"<<endl;
        cin>>title;
        cout<<"enter the priceof the tape"<<endl;
        cin>>price;
        cout<<"enter the pages of the playing time(in minutes):"<<endl;
        cin>>min;
        try
        {
                if(min>30.00 && min<90.00)
                {
                    if(price>500 && price <1000)
                    {
                         displayt();
                         }
                }
                else throw(min);
            }
            catch(float f){
                cout<<"caught exception in function of tape:"<<endl;
                cout<<"you entered  an invalid data"<<endl;
                title="0";
                price=0.0;
                min=0.0;
                displayt();
                throw;
            }
    }

                void displayt()
                {

                    cout<<"the deatial of tape you ented is"<<endl;
                    cout<<"the entered title is"<<title<<endl;
                    cout<<"the entered price is"<<price<<endl;
                    cout<<"the entered play time is: "<<min<<endl;
                }

            };
            int main(){
        
            books b;
            tape t;
            int choice;
            cout<<"Do you want to buy a book(prees 1)or a tape(press 2):"<<flush;
            cin>>choice;
            switch(choice)
            {
                case 1: try  
                    {
                      b.getb();  
                    }
                    catch(int i){
                        cout<<"Caught exception in int main()"<<endl;
                    }break;
                case 2: try   
                {
                    t.gett();
                } 
                catch(float f){
                    cout<<"Caught Exception in int main()";
                }break;
                    default:cout<<"entered bad choice!Try again!!"<<endl;
                }
                return 0;               
            }        






/********************************************************************/


/*OUTPUT:-

PS F:\cpp\oops\Assignment> ./a.exe
Do you want to buy a book(prees 1)or a tape(press 2):1
Enter the details of the book:
Enter the title of the book:
THEMAN
Enter the price of the book:
200
Enter the pages of the book:
366
Caught the exeption in the functiion of the book
You entered an invalid data
The book you enetred is

the enterd title is:0
the enterd price is:0
the enterd pages are:0
PS F:\cpp\oops\Assignment> ./a.exe     
Do you want to buy a book(prees 1)or a tape(press 2):2
Enter the details of the tape
enter the title of the tape
tapereal
enter the priceof the tape
600
enter the pages of the playing time(in minutes):
80
the deatial of tape you ented is
the entered title istapereal
the entered price is600
the entered play time is: 80

*/
