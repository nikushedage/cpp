/*Implement a class Complex which represents the complex Number data type.
implent the following operations:
1.Constructor (includung a default constructor which creates the complex number 0+0i)
2.overloaded operator + to add two complex numbers.
3.overload operator * to multiply two complex numbers.
4. Overload <<and >> to print and read Complex numbers.*/


#include<iostream>
using namespace std;
class complex
{
 float x;
 float y;
public:
complex()
{
  x=0;
  y=0;
}
  complex operator+(complex);
  complex operator*(complex);
friend istream &operator >>(istream &input,complex& t)
{
  cout<<"enter the real part";
  input>>t.x;
  cout<<"Enter the imaginary part part ";
  input>>t.y;
  return input;
 }
friend ostream &operator <<(ostream &output,complex& t)
{
  output<<t.x<<"+"<<t.y<<"i";
  return output;
  }
};
complex complex::operator+(complex c)
{
  complex temp;
  temp.x = x + c.x;
  temp.y = y + c.y;
  return(temp);
}
complex complex::operator*(complex c)
{
  complex temp2;
  temp2.x=(x*c.x)-(y*c.y);
  temp2.y=(y*c.x)+(x*c.y);
  return(temp2);
}
int main()
{
   complex c1,c2,c3,c4;
   cout<<"Default constructor value = \n";
   cout<<c1;
   cout<<"\nEnter the 1st number\n";
   cin>>c1;
   cout<<"\nEnter the 2nd number\n";
   cin>>c2;
   c3=c1+c2;
   c4=c1*c2;
   cout<<"\nThe first number is ";
   cout<<c1;
   cout<<"\nThe second number is ";
   cout<<c2;
   cout<<"\nThe addition is ";
   cout<<c3;
   cout<<"\nThe multiplication is ";
   cout<<c4;
   return 0;
}


/* F:\cpp\oops\Assignment> ./a.exe
Default constructor value = 
0+0i
Enter the 1st number        
enter the real part2
Enter the imaginary part part 4

Enter the 2nd number
enter the real part4
Enter the imaginary part part 8

The first number is 2+4i     
The second number is 4+8i    
The addition is 6+12i        
The multiplication is -24+32i*/
