/* Develope an object oriented program in c++ to create adatabase of student information system containg the information . Name , Roll no. class, division, Date of birth,Blood group, contact address, telephone number , driving license no , etc. Construct the data base with suitable member function for initializing and destroyingthe data via Constructor, default constuctor, copy constructor, destructor , static member function, friend class, this pointer,inline code and dynamic memory allocation operators-new and delete.*/


#include<iostream>
#include<string.h>
using namespace std;

class person_additional_info
{
    char address[20],license[20],insurance[20];
    long int contact;
      public:
        person_additional_info()
      {
        strcpy(address,"XYZ");
        strcpy(license,"XY-0000000000");
        strcpy(insurance,"XY00000000X");
        contact=0000000000;
      }
      ~person_additional_info()
      {
        cout<<"i am in destructor";
      }
      friend class person;
};
class person
{ 
    char name[20],dob[10],blood[10],insurance[20],license[20];
    float ht,wt;
    static int count;
    person_additional_info *pai;

    public:
      person()
    {
        strcpy(name," XYZ ");
        strcpy(dob," dd/mm/yy ");
        strcpy(blood," A + ");
        ht=0;
        wt=0;
          pai=new person_additional_info;
    }
      person(person*p1)
    {
        strcpy(name,p1 ->name);
        strcpy(dob,p1->dob);
        strcpy(blood,p1->blood);
        ht=p1->ht;
        wt=p1->wt;
          pai=new person_additional_info;
          strcpy(pai->address,p1->pai->address);
          strcpy(pai->license,p1->pai->license);
          strcpy(pai->insurance,p1->pai->insurance);
          pai->contact=p1->pai->contact;
    }
    static void showcount()
    {
        cout<<"\nNo of records count= "<<count<<"\n";
    }
       ~person()
    {
        cout<<" \nI am in destructor\n ";
    }
      void getdata(char name[20]);
      inline void display();
    };
    void person::getdata(char name [20])
    {
        strcpy(this->name,name);
        cout<<"\n enter date of birth ";
        cin>>dob;
        cout<<"\n enter the blood group ";
        cin>>blood;
        cout<<"\n enter height ";
         cin>>ht;
         cout<<"\n enter the weight ";
         cin>>wt;
         cout<<"\n enter the address ";
         cin>>pai->address;
         cout<<"\n enter license number ";
         cin>>license;
         cout<<"\n enter the insurance policy number ";
         cin>>insurance;
         cout<<"\n enter contact number ";
        cin>>pai->contact;
        count++;
    }
    void person::display()
    {
        cout<<"\t"<<name;
        cout<<"\t"<<dob;
        cout<<"\t"<<blood;
        cout<<"\t"<<ht;
        cout<<"\t"<<wt;
        cout<<"\t"<<pai->address;
        cout<<"\t"<<pai->license;
        cout<<"\t"<<pai->insurance;
        cout<<"\t"<<pai->contact;
    }
    int person::count;
    int main()
    {
        person *p1,*p2;
        int ch;
        p1=new person;
        p2=new person(p1);
        cout<<"\tName";
        cout<<"\tdob";
        cout<<"\tht";
        cout<<"\twt";
        cout<<"\tAddress";
        cout<<"\tlicense";
        cout<<"\tinsurance";
        cout<<"\tcontact";
        cout<<endl;
        cout<<"\tDefault constructor value\n";
        p1->display();
        cout<<"\n";
        cout<<"\tcopy constructor value\n";
        p2->display();
        int n;
        cout<<"\nEnter how many records you want?\n";
        cin>>n;
        person p3[n];
        char name[20];
        int x=0;
        do
        {
          cout<<"\nWelcome to  person data base system ";
          cout<<"\n1.Enter the record ";
          cout<<"\n2.Display the record ";
          cout<<"\n3.Exit ";
          cin>>ch;
          switch(ch)
          {
            case 1:
            {
               cout<<"\nEnter the name: ";
               cin>>name;
               p3[x].getdata(name);
               person::showcount();
               x++;
            break;
          }
            case 2:
            {
            cout<<"\tName "<<endl;
            cout<<"\tdobName ";
            cout<<"\tBlood ";
            cout<<"\tht ";
            cout<<"\twt ";
            cout<<"\tAddress ";
            cout<<"\tlisence ";
            cout<<"\tinsurance ";
            cout<<"\tcontact";
            for(int i=0;i<n;i++)
            {
                cout<<"\n";
                p3[i].display();

            }
            break;
          }
        } 
        } while(ch!=3);
        delete p1;
        delete p2;
        return 0;
        }
    
    
/********************************************************************/


/*OUTPUT:-



 Name    dob     ht      wt      Address license insurance       contact       
        Default constructor value
         XYZ     dd/mm/yy  A +   A +    0       0       XYZ     XY-0000000000   XY00000000X   0
        copy constructor value
         XYZ     dd/mm/yy  A +   A +    0       0       XYZ     XY-0000000000   XY00000000X   0
Enter how many records you want?
2

Welcome to  person data base system 
1.Enter the record 
2.Display the record 
3.Exit 1

Enter the name: nikita

 enter date of birth 12/02/2004

 enter the blood group O-

 enter height 6

 enter the weight 45

 enter the address pune

 enter license number ns123

 enter the insurance policy number nj2879

 enter contact number 775791813

No of records count= 1

Welcome to  person data base system
1.Enter the record
2.Display the record
3.Exit 2
        Name 
        dobName         Blood   ht      wt      Address         lisence         insurance     contact
        nikita  12/02/2004O-    O-      6       45      pune    XY-0000000000   XY00000000X   775791813
         XYZ     dd/mm/yy  A +   A +    0       0       XYZ     XY-0000000000   XY00000000X   0
Welcome to  person data base system
1.Enter the record
2.Display the record
3.Exit 3
 
I am in destructor

I am in destructor

I am in destructor

I am in destructor
*/
